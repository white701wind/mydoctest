package com.mw.mydoctest.ui.diagnostics

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mw.mydoctest.R
import com.mw.mydoctest.core.repository.data.DataDiagnostic
import com.squareup.picasso.Picasso

class DiagnosticsAdapter(private val onItemClickListener: OnItemClickListener) :
    PagedListAdapter<DataDiagnostic, DiagnosticsAdapter.DiagnosticViewHolder>(
        DIFF_CALLBACK
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiagnosticViewHolder {
        return DiagnosticViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_diagnostics, parent, false),
            onItemClickListener
        )
    }

    class DiagnosticViewHolder(itemView: View, private val onItemClickListener: OnItemClickListener) : RecyclerView.ViewHolder(itemView) {

        private val nameTextView = itemView.findViewById<TextView>(R.id.nameTextView)
        private val descriptionTextView = itemView.findViewById<TextView>(R.id.descriptionTextView)
        private val iconImageView = itemView.findViewById<ImageView>(R.id.imageView)

        init {
            itemView.setOnClickListener {
                onItemClickListener.onClickListener(it, adapterPosition)
            }
        }

        fun bindTo(dataDiagnostic: DataDiagnostic) {
            nameTextView.text = dataDiagnostic.name
            descriptionTextView.text = dataDiagnostic.description
            Picasso.get()
                .load(dataDiagnostic.icon)
                .resizeDimen(R.dimen.list_detail_image_size, R.dimen.list_detail_image_size)
                .centerInside()
                .tag(itemView.context)
                .into(iconImageView)
        }
    }

    override fun onBindViewHolder(holder: DiagnosticViewHolder, position: Int) {
        getItem(position)?.let { holder.bindTo(it) }
    }

    companion object {
        private val DIFF_CALLBACK = object :
            DiffUtil.ItemCallback<DataDiagnostic>() {
            // Concert details may have changed if reloaded from the database,
            // but ID is fixed.
            override fun areItemsTheSame(oldConcert: DataDiagnostic, newConcert: DataDiagnostic) =
                oldConcert.id_server == newConcert.id_server

            override fun areContentsTheSame(oldConcert: DataDiagnostic, newConcert: DataDiagnostic) =
                oldConcert == newConcert
        }
    }
}

interface OnItemClickListener {
    fun onClickListener(view: View, position: Int)
}