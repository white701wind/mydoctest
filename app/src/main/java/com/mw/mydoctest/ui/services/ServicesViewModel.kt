package com.mw.mydoctest.ui.services

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mw.mydoctest.core.repository.data.DataService
import com.mw.mydoctest.core.repository.Repository
import com.mw.mydoctest.core.repository.datasource.ServiceDataSource
import com.mw.mydoctest.core.repository.converter.ConverterApiToData
import com.mw.mydoctest.core.repository.db.RoomDatabaseClient
import com.mw.mydoctest.core.repository.httpclient.RetrofitHttpClient

class ServicesViewModel(
    private val repository: Repository
) :
    ViewModel(), ServiceDataSource.ObserverStateLoadData {

    private var dataServicePagedListLiveData: LiveData<PagedList<DataService>>

    private val selectDataServiceMutableLiveData: MutableLiveData<DataService> = MutableLiveData()
    fun getSelectServiceLiveData(): LiveData<DataService> = selectDataServiceMutableLiveData

    private val stateLoadDataMutableLiveData: MutableLiveData<Boolean> = MutableLiveData(false)
    fun getStateLoadDataLiveData(): LiveData<Boolean> = stateLoadDataMutableLiveData

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(4)
            .setEnablePlaceholders(false)
            .build()
        dataServicePagedListLiveData = initializedPagedListBuilder(config).build()
    }

    fun getServicePagedListLiveData(): LiveData<PagedList<DataService>> =
        dataServicePagedListLiveData

    private fun initializedPagedListBuilder(config: PagedList.Config):
            LivePagedListBuilder<Int, DataService> {

        val dataSourceFactory = object : DataSource.Factory<Int, DataService>() {
            override fun create(): DataSource<Int, DataService> {
                return ServiceDataSource(
                    viewModelScope,
                    repository,
                    this@ServicesViewModel
                )
            }
        }
        return LivePagedListBuilder<Int, DataService>(dataSourceFactory, config)
    }

    fun selectServiceIndex(index: Int) {
        dataServicePagedListLiveData.value?.let {
            selectDataServiceMutableLiveData.value = it[index]
        }
    }

    override fun startLoad() {
        stateLoadDataMutableLiveData.value = true
    }

    override fun endLoad() {
        stateLoadDataMutableLiveData.value = false
    }
}
