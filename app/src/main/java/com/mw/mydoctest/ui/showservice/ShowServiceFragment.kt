package com.mw.mydoctest.ui.showservice


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.mw.mydoctest.R
import com.mw.mydoctest.ui.services.ServicesViewModel
import kotlinx.android.synthetic.main.fragment_show_diagnostic.*

/**
 * A simple [Fragment] subclass.
 */
class ShowServiceFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_show_service, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewModel = ViewModelProviders.of(activity!!).get(ServicesViewModel::class.java)
        viewModel.getSelectServiceLiveData().observe(this, Observer {
            (activity as AppCompatActivity).supportActionBar?.title = it.name
            descriptionTextView.text = it.description
        })
    }
}
