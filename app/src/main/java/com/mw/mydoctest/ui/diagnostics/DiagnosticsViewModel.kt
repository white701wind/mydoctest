package com.mw.mydoctest.ui.diagnostics

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mw.mydoctest.core.repository.data.DataDiagnostic
import com.mw.mydoctest.core.repository.datasource.DiagnosticDataSource

import com.mw.mydoctest.core.repository.Repository
import com.mw.mydoctest.core.repository.converter.ConverterApiToData
import com.mw.mydoctest.core.repository.db.RoomDatabaseClient
import com.mw.mydoctest.core.repository.httpclient.RetrofitHttpClient

class DiagnosticsViewModel(
    private val repository: Repository
) :
    ViewModel(), DiagnosticDataSource.ObserverStateLoadData {

    private var dataDiagnosticPagedListLiveData: LiveData<PagedList<DataDiagnostic>>

    private val selectDataDiagnosticMutableLiveData: MutableLiveData<DataDiagnostic> =
        MutableLiveData()

    fun getSelectDiagnosticLiveData(): LiveData<DataDiagnostic> =
        selectDataDiagnosticMutableLiveData

    private val stateLoadDataMutableLiveData: MutableLiveData<Boolean> = MutableLiveData(false)
    fun getStateLoadDataLiveData(): LiveData<Boolean> = stateLoadDataMutableLiveData

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(4)
            .setEnablePlaceholders(false)
            .build()
        dataDiagnosticPagedListLiveData = initializedPagedListBuilder(config).build()
    }

    fun getDiagnosticPagedListLiveData(): LiveData<PagedList<DataDiagnostic>> =
        dataDiagnosticPagedListLiveData

    private fun initializedPagedListBuilder(config: PagedList.Config):
            LivePagedListBuilder<Int, DataDiagnostic> {

        val dataSourceFactory = object : DataSource.Factory<Int, DataDiagnostic>() {
            override fun create(): DataSource<Int, DataDiagnostic> {
                return DiagnosticDataSource(
                    viewModelScope,
                    repository,
                    this@DiagnosticsViewModel
                )
            }
        }
        return LivePagedListBuilder<Int, DataDiagnostic>(dataSourceFactory, config)
    }

    fun selectDiagnosticIndex(index: Int) {
        dataDiagnosticPagedListLiveData.value?.let {
            selectDataDiagnosticMutableLiveData.value = it[index]
        }
    }

    override fun startLoad() {
        stateLoadDataMutableLiveData.value = true
    }

    override fun endLoad() {
        stateLoadDataMutableLiveData.value = false
    }
}
