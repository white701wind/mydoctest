package com.mw.mydoctest.ui.services

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mw.mydoctest.R
import com.mw.mydoctest.core.repository.data.DataService
import com.squareup.picasso.Picasso

class ServicesAdapter(private val onItemClickListener: OnItemClickListener) :
    PagedListAdapter<DataService, ServicesAdapter.ServicesViewHolder>(
        DIFF_CALLBACK
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServicesViewHolder {
        return ServicesViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_services, parent, false),
            onItemClickListener
        )
    }

    class ServicesViewHolder(itemView: View, private val onItemClickListener: OnItemClickListener) : RecyclerView.ViewHolder(itemView) {

        private val nameTextView = itemView.findViewById<TextView>(R.id.nameTextView)
        private val descriptionTextView = itemView.findViewById<TextView>(R.id.descriptionTextView)
        private val iconImageView = itemView.findViewById<ImageView>(R.id.imageView)

        init {
            itemView.setOnClickListener {
                onItemClickListener.onClickListener(it, adapterPosition)
            }
        }

        fun bindTo(dataDiagnostic: DataService) {
            nameTextView.text = dataDiagnostic.name
            descriptionTextView.text = dataDiagnostic.description
            Picasso.get()
                .load(dataDiagnostic.icon)
                .resizeDimen(R.dimen.list_detail_image_size, R.dimen.list_detail_image_size)
                .centerInside()
                .tag(itemView.context)
                .into(iconImageView)
        }
    }

    override fun onBindViewHolder(holder: ServicesViewHolder, position: Int) {
        getItem(position)?.let { holder.bindTo(it) }
    }

    companion object {
        private val DIFF_CALLBACK = object :
            DiffUtil.ItemCallback<DataService>() {
            // Concert details may have changed if reloaded from the database,
            // but ID is fixed.
            override fun areItemsTheSame(oldConcert: DataService, newConcert: DataService) =
                oldConcert.id_server == newConcert.id_server

            override fun areContentsTheSame(oldConcert: DataService, newConcert: DataService) =
                oldConcert == newConcert
        }
    }
}

interface OnItemClickListener {
    fun onClickListener(view: View, position: Int)
}