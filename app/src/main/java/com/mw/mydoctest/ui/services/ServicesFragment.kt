package com.mw.mydoctest.ui.services

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mw.mydoctest.R
import com.mw.mydoctest.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_services.*

class ServicesFragment : Fragment(), OnItemClickListener {

    private lateinit var viewModel: ServicesViewModel
    private val servicesAdapter = ServicesAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_services, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        recyclerView.adapter = servicesAdapter

        viewModel = ViewModelProviders.of(activity!!, ViewModelFactory()).get(ServicesViewModel::class.java)
        viewModel.getServicePagedListLiveData().observe(this, Observer {
            servicesAdapter.submitList(it)
        })

        viewModel.getStateLoadDataLiveData().observe(this, Observer {
            progressBar.visibility = if (it) View.VISIBLE else View.INVISIBLE
        })
    }

    override fun onClickListener(view: View, position: Int) {
        viewModel.selectServiceIndex(position)
        val navController = findNavController()
        navController.navigate(R.id.action_nav_home_to_showServiceFragment)
    }
}
