package com.mw.mydoctest.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.mw.mydoctest.R
import com.mw.mydoctest.ui.diagnostics.DiagnosticsFragment
import com.mw.mydoctest.ui.services.ServicesFragment
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val nameTabList: List<String> = listOf("Медицинские", "Диагностические")

        val viewPagerAdapter = ViewPagerAdapter(childFragmentManager, lifecycle)
        viewPagerAdapter.addFragment(ServicesFragment())
        viewPagerAdapter.addFragment(DiagnosticsFragment())
        viewPager2.adapter = viewPagerAdapter
        viewPager2.orientation = ViewPager2.ORIENTATION_HORIZONTAL

        TabLayoutMediator(tabs, viewPager2,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                tab.text = nameTabList[position]
            }).attach()
    }
}