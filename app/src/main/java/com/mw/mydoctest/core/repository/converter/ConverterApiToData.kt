package com.mw.mydoctest.core.repository.converter

import com.mw.mydoctest.core.repository.data.DataDiagnostic
import com.mw.mydoctest.core.repository.data.DataService
import com.mw.mydoctest.core.repository.apidata.diagnostics.ApiDiagnostics
import com.mw.mydoctest.core.repository.apidata.services.ApiServices
import com.mw.mydoctest.core.repository.db.EntityDiagnostic
import com.mw.mydoctest.core.repository.db.EntityService
import javax.inject.Inject

class ConverterApiToData @Inject constructor() {

    fun toDataDiagnosticList(apiDiagnostics: ApiDiagnostics): List<DataDiagnostic> {
        return apiDiagnostics.collection.map {
            DataDiagnostic(
                it.name,
                it.description ?: "",
                "https://mydoc.ru" + it.icon.urls.circle,
                it.id
            )
        }
    }

    fun toTotalCount(apiDiagnostics: ApiDiagnostics): Int {
        return apiDiagnostics.total_count
    }

    fun toDataServiceList(apiServices: ApiServices): List<DataService> {
        return apiServices.collection.map {
            DataService(
                it.name,
                it.description ?: "",
                "https://mydoc.ru" + it.icon.urls.circle,
                it.id
            )
        }
    }

    fun toTotalCount(apiServices: ApiServices): Int {
        return apiServices.total_count
    }

    fun toEntityDiagnosticList(apiDiagnostics: ApiDiagnostics): List<EntityDiagnostic> {
        return apiDiagnostics.collection.map {
            EntityDiagnostic(
                0,
                it.name,
                it.description ?: "",
                "https://mydoc.ru" + it.icon.urls.circle,
                it.id
            )
        }
    }

    fun toEntityServiceList(apiServices: ApiServices): List<EntityService> {
        return apiServices.collection.map {
            EntityService(
                0,
                it.name,
                it.description ?: "",
                "https://mydoc.ru" + it.icon.urls.circle,
                it.id
            )
        }
    }

    fun toDataDiagnosticList(entityDiagnosticList: List<EntityDiagnostic>): List<DataDiagnostic> {
        return entityDiagnosticList.map {
            DataDiagnostic(
                it.name,
                it.description ?: "",
                it.icon,
                it.id_server
            )
        }
    }

    fun toDataServiceList(entityServiceList: List<EntityService>): List<DataService> {
        return entityServiceList.map {
            DataService(
                it.name,
                it.description ?: "",
                it.icon,
                it.id_server
            )
        }
    }
}