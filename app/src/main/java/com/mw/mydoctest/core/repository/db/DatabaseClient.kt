package com.mw.mydoctest.core.repository.db

import android.content.Context
import androidx.room.Query
import androidx.room.Room
import javax.inject.Inject
import javax.inject.Named

interface DatabaseClient {
    suspend fun insertDiagnosticList(entityDiagnosticList: List<EntityDiagnostic>)
    suspend fun diagnostics(limit: Int, indexInResponse: Int): List<EntityDiagnostic>
    suspend fun diagnosticsCount(): Int

    suspend fun insertServiceList(entityServiceList: List<EntityService>)
    suspend fun services(limit: Int, indexInResponse: Int): List<EntityService>
    suspend fun servicesCount(): Int
}

class RoomDatabaseClient @Inject constructor(@Named("ApplicationContext") context: Context): DatabaseClient {

    val database = Room.databaseBuilder(context, MyDocRoom::class.java, "mydoc.db").build()

    override suspend fun insertDiagnosticList(entityDiagnosticList: List<EntityDiagnostic>) {
        database.newsDao().insertDiagnosticList(entityDiagnosticList)
    }

    override suspend fun diagnostics(limit: Int, indexInResponse: Int): List<EntityDiagnostic> {
        return database.newsDao().diagnostics(limit, indexInResponse)
    }

    override suspend fun diagnosticsCount(): Int {
        return database.newsDao().diagnosticsCount()
    }

    override suspend fun insertServiceList(entityServiceList: List<EntityService>) {
        database.newsDao().insertServiceList(entityServiceList)
    }

    override suspend fun services(limit: Int, indexInResponse: Int): List<EntityService> {
        return database.newsDao().services(limit, indexInResponse)
    }

    override suspend fun servicesCount(): Int {
        return database.newsDao().servicesCount()
    }
}