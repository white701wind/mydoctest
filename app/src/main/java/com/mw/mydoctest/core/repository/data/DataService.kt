package com.mw.mydoctest.core.repository.data

data class DataService(
    val name: String,
    val description: String,
    val icon: String,
    val id_server: Int
)