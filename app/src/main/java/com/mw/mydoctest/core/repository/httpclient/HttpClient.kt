package com.mw.mydoctest.core.repository.httpclient

import com.mw.mydoctest.core.repository.ResultApiMyDoc
import com.mw.mydoctest.core.repository.apidata.diagnostics.ApiDiagnostics
import com.mw.mydoctest.core.repository.apidata.services.ApiServices
import retrofit2.Response
import javax.inject.Inject

interface HttpClient {
    suspend fun diagnostics(limit: Int, page: Int): ResultApiMyDoc<ApiDiagnostics>
    suspend fun services(limit: Int, page: Int): ResultApiMyDoc<ApiServices>
}

class RetrofitHttpClient @Inject constructor(): HttpClient {

    val apiMyDoc: ApiMyDoc = ApiMyDoc.create()

    override suspend fun diagnostics(limit: Int, page: Int): ResultApiMyDoc<ApiDiagnostics> {
        val response: Response<ApiDiagnostics>
        try {
            response = apiMyDoc.diagnostics(limit = limit, page = page)
            when {
                response.isSuccessful -> {
                    val apiDiagnostics = response.body() as ApiDiagnostics
                    return ResultApiMyDoc.Success(apiDiagnostics)
                }
                else -> {
                    return ResultApiMyDoc.Error("error query diagnostics")
                }
            }
        } catch (e: Exception) {
            return ResultApiMyDoc.Error("error query diagnostics")
        }
    }

    override suspend fun services(limit: Int, page: Int): ResultApiMyDoc<ApiServices> {
        val response: Response<ApiServices>
        try {
            response = apiMyDoc.services(limit = limit, page = page)
            when {
                response.isSuccessful -> {
                    val apiServices = response.body() as ApiServices
                    return ResultApiMyDoc.Success(apiServices)
                }
                else -> {
                    return ResultApiMyDoc.Error("error query services")
                }
            }
        } catch (e: Exception) {
            return ResultApiMyDoc.Error("error query services")
        }
    }
}
