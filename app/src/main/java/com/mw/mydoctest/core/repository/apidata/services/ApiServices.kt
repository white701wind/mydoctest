package com.mw.mydoctest.core.repository.apidata.services

import com.mw.mydoctest.core.repository.apidata.diagnostics.Collection
import com.mw.mydoctest.core.repository.apidata.diagnostics.Icon
import com.mw.mydoctest.core.repository.apidata.diagnostics.Urls

data class ApiServices(
    val collection: List<Collection>,
    val current_page: Int,
    val msg: Any,
    val per_page: Int,
    val total_count: Int,
    val type: String
)

data class Collection(
    val children_count: Int,
    val created_at: String,
    val db_id: Int,
    val depth: Int,
    val description: String,
    val icon: Icon,
    val id: Int,
    val klass: String,
    val name: String,
    val parent_id: Int,
    val provider_id: String,
    val serializer_type: String,
    val short: String,
    val slug: String,
    val updated_at: String
)

data class Icon(
    val content_type: String,
    val filename: String,
    val human: String,
    val id: Int,
    val klass: String,
    val serializer_type: String,
    val type: String,
    val urls: Urls
)

data class Urls(
    val circle: String,
    val original: String,
    val web_200: String
)