package com.mw.mydoctest.core.repository.httpclient

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.mw.mydoctest.core.repository.apidata.diagnostics.ApiDiagnostics
import com.mw.mydoctest.core.repository.apidata.services.ApiServices
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiMyDoc {

    @GET("diagnostics.json")
    suspend fun diagnostics(
        @Query("limit") limit: Int,
        @Query("page") page: Int,
        @Query("filter[depth]") depth: Int = 1,
        @Query("filter[root]") root: Boolean = true,
        @Query("fields") fields: String = "icon",
        @Query("filter[cities][identifier]") ci: Int = 1
    ): Response<ApiDiagnostics>

    @GET("services.json")
    suspend fun services(
        @Query("limit") limit: Int,
        @Query("page") page: Int,
        @Query("filter[depth]") depth: Int = 1,
        @Query("group_type") group_type: String = "tree",
        @Query("fields") fields: String = "icon",
        @Query("filter[cities][identifier]") ci: Int = 1
    ): Response<ApiServices>

    companion object Factory {

        private val baseUrl = "https://mydoc.ru/api/ios/medical_providers/docdoc/"

        fun create(): ApiMyDoc {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .baseUrl(baseUrl)
                .build()

            return retrofit.create(ApiMyDoc::class.java)
        }
    }
}


