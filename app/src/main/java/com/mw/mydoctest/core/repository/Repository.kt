package com.mw.mydoctest.core.repository

import com.mw.mydoctest.core.repository.converter.ConverterApiToData
import com.mw.mydoctest.core.repository.data.DataDiagnostic
import com.mw.mydoctest.core.repository.data.DataService
import com.mw.mydoctest.core.repository.db.DatabaseClient
import com.mw.mydoctest.core.repository.db.EntityDiagnostic
import com.mw.mydoctest.core.repository.db.EntityService
import com.mw.mydoctest.core.repository.httpclient.HttpClient
import javax.inject.Inject

class Repository @Inject constructor(
    private val httpClient: HttpClient,
    private val converterApiToData: ConverterApiToData,
    private val databaseClient: DatabaseClient
) {

    var totalCountDiagnostic: Int = 0
    var totalCountService: Int = 0

    suspend fun diagnostics(limit: Int, page: Int): List<DataDiagnostic> {
        return when (val result = httpClient.diagnostics(limit, page)) {
            is ResultApiMyDoc.Success -> {
                val dataDiagnosticList: List<DataDiagnostic> =
                    converterApiToData.toDataDiagnosticList(result.value)
                totalCountDiagnostic = converterApiToData.toTotalCount(result.value)

                val entityDiagnosticList: List<EntityDiagnostic> =
                    converterApiToData.toEntityDiagnosticList(result.value)

                val indexInResponse = limit * page - limit
                entityDiagnosticList.forEachIndexed {index, entityDiagnostic ->
                    entityDiagnostic.indexInResponse = index + indexInResponse
                }

                databaseClient.insertDiagnosticList(entityDiagnosticList)

                dataDiagnosticList
            }
            is ResultApiMyDoc.Error -> {
                println(result.message)

                val indexInResponse = limit * page - limit
                val entityDiagnosticList: List<EntityDiagnostic> = databaseClient.diagnostics(limit, indexInResponse)
                val dataDiagnosticList: List<DataDiagnostic> = converterApiToData.toDataDiagnosticList(entityDiagnosticList)
                totalCountDiagnostic = databaseClient.diagnosticsCount()
                dataDiagnosticList
            }
        }
    }

    suspend fun services(limit: Int, page: Int): List<DataService> {
        return when (val result = httpClient.services(limit, page)) {
            is ResultApiMyDoc.Success -> {
                val dataServiceList: List<DataService> =
                    converterApiToData.toDataServiceList(result.value)
                totalCountService = converterApiToData.toTotalCount(result.value)

                val entityServiceList: List<EntityService> =
                    converterApiToData.toEntityServiceList(result.value)

                val indexInResponse = limit * page - limit
                entityServiceList.forEachIndexed { index, entityService ->
                    entityService.indexInResponse = index + indexInResponse
                }

                databaseClient.insertServiceList(entityServiceList)

                dataServiceList
            }
            is ResultApiMyDoc.Error -> {
                println(result.message)

                val indexInResponse = limit * page - limit
                val entityServiceList: List<EntityService> = databaseClient.services(limit, indexInResponse)
                val dataServiceList: List<DataService> = converterApiToData.toDataServiceList(entityServiceList)
                totalCountService = databaseClient.servicesCount()

                dataServiceList
            }
        }
    }
}