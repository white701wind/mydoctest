package com.mw.mydoctest.core.repository.db

import androidx.room.*

@Dao
interface MyDocDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entityDiagnostic: EntityDiagnostic)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entityService: EntityService)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDiagnosticList(entityDiagnosticList: List<EntityDiagnostic>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertServiceList(entityServiceList: List<EntityService>)

    @Query("SELECT * FROM diagnostics WHERE indexInResponse >= :indexInResponse ORDER BY indexInResponse limit :limit")
    suspend fun diagnostics(limit: Int, indexInResponse: Int): List<EntityDiagnostic>

    @Query("SELECT * FROM services WHERE indexInResponse >= :indexInResponse ORDER BY indexInResponse limit :limit")
    suspend fun services(limit: Int, indexInResponse: Int): List<EntityService>

    @Query("SELECT COUNT(*) FROM diagnostics")
    suspend fun diagnosticsCount(): Int

    @Query("SELECT COUNT(*) FROM services")
    suspend fun servicesCount(): Int
}

//@Dao
//interface NewsDao {
//
//    @Query("SELECT * FROM news ORDER BY time DESC")
//    suspend fun dbNewsList(): List<DbNews>
//
//    @Query("SELECT * FROM news WHERE favorites = 1  ORDER BY time DESC")
//    suspend fun dbNewsFavoritesList(): List<DbNews>
//
//    @Query("SELECT * FROM news WHERE id_server = :id_server")
//    suspend fun getNewsById(id_server: Long): DbNews
//
//    @Query("SELECT MAX(time) FROM news")
//    suspend fun dbNewsLastTime(): Long
//
//    @Query("SELECT COUNT(*) FROM news")
//    suspend fun dbNewsCount(): Long
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    suspend fun insert(dbNews: DbNews)
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    suspend fun insert(dbNewsList: List<DbNews>)
//
//    @Update
//    suspend fun update(dbNews: DbNews)
//
//}