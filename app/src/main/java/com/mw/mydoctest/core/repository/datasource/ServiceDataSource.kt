package com.mw.mydoctest.core.repository.datasource

import androidx.paging.PageKeyedDataSource
import com.mw.mydoctest.core.repository.Repository
import com.mw.mydoctest.core.repository.data.DataService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class ServiceDataSource(
    private val scope: CoroutineScope,
    private val repository: Repository,
    private val observerStateLoadData: ObserverStateLoadData
) :
    PageKeyedDataSource<Int, DataService>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, DataService>
    ) {
        scope.launch {
            observerStateLoadData.startLoad()
            val list = repository.services(params.requestedLoadSize, 1)
            val nextPage =
                if (params.requestedLoadSize < repository.totalCountService) 4 else null
            callback.onResult(list, null, nextPage)
            observerStateLoadData.endLoad()
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, DataService>) {
        scope.launch {
            observerStateLoadData.startLoad()
            val list = repository.services(params.requestedLoadSize, params.key)
            val nextPage =
                if (params.requestedLoadSize * params.key < repository.totalCountService) params.key + 1 else null
            callback.onResult(list, nextPage)
            observerStateLoadData.endLoad()
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, DataService>) {
        scope.launch {
            observerStateLoadData.startLoad()
            val list = repository.services(params.requestedLoadSize, params.key)
            val prevPage =
                if (params.requestedLoadSize * params.key - params.requestedLoadSize > 0) params.key - 1 else null
            callback.onResult(list, prevPage)
            observerStateLoadData.endLoad()
        }
    }

    override fun invalidate() {
        super.invalidate()
        scope.cancel()
    }

    interface ObserverStateLoadData {
        fun startLoad()
        fun endLoad()
    }
}