package com.mw.mydoctest.core.repository

sealed class ResultApiMyDoc<out T : Any> {
    data class Success<out T : Any>(val value: T) : ResultApiMyDoc<T>()
    data class Error(val message: String, val cause: Exception? = null) : ResultApiMyDoc<Nothing>()
}