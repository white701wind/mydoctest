package com.mw.mydoctest.core.repository.db

import androidx.room.*

@Database(entities = [EntityService::class, EntityDiagnostic::class], version = 1)
abstract class MyDocRoom : RoomDatabase() {

    abstract fun newsDao(): MyDocDao
}

@Entity(tableName = "diagnostics" )
class EntityDiagnostic(
    @ColumnInfo(name = "indexInResponse") var indexInResponse: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "icon") val icon: String,
    @PrimaryKey
    @ColumnInfo(name = "id_server") val id_server: Int
) {
    constructor() : this(0, "", "", "", 0)
}

@Entity(tableName = "services" )
class EntityService(
    @ColumnInfo(name = "indexInResponse") var indexInResponse: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "icon") val icon: String,
    @PrimaryKey
    @ColumnInfo(name = "id_server") val id_server: Int
) {
    constructor() : this(0, "", "", "", 0)
}
