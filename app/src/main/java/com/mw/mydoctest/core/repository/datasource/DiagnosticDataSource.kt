package com.mw.mydoctest.core.repository.datasource

import androidx.paging.PageKeyedDataSource
import com.mw.mydoctest.core.repository.Repository
import com.mw.mydoctest.core.repository.data.DataDiagnostic
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class DiagnosticDataSource(
    private val scope: CoroutineScope,
    private val repository: Repository,
    private val observerStateLoadData: ObserverStateLoadData
) :
    PageKeyedDataSource<Int, DataDiagnostic>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, DataDiagnostic>
    ) {
        scope.launch {
            observerStateLoadData.startLoad()
            val list = repository.diagnostics(params.requestedLoadSize, 1)
            val nextPage =
                if (params.requestedLoadSize < repository.totalCountDiagnostic) 4 else null
            callback.onResult(list, null, nextPage)
            observerStateLoadData.endLoad()
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, DataDiagnostic>) {
        scope.launch {
            observerStateLoadData.startLoad()
            val list = repository.diagnostics(params.requestedLoadSize, params.key)
            val nextPage =
                if (params.requestedLoadSize * params.key < repository.totalCountDiagnostic) params.key + 1 else null
            callback.onResult(list, nextPage)
            observerStateLoadData.endLoad()
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, DataDiagnostic>) {
        scope.launch {
            observerStateLoadData.startLoad()
            val list = repository.diagnostics(params.requestedLoadSize, params.key)
            val prevPage =
                if (params.requestedLoadSize * params.key - params.requestedLoadSize > 0) params.key - 1 else null
            callback.onResult(list, prevPage)
            observerStateLoadData.endLoad()
        }
    }

    override fun invalidate() {
        super.invalidate()
        scope.cancel()
    }

    interface ObserverStateLoadData {
        fun startLoad()
        fun endLoad()
    }
}