package com.mw.mydoctest.core.repository.data

data class DataDiagnostic(
    val name: String,
    val description: String,
    val icon: String,
    val id_server: Int
)