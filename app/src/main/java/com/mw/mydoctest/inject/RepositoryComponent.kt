package com.mw.alfanews.inject

import com.mw.mydoctest.viewmodel.ViewModelFactory
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ModuleContext::class, ModuleHttpClient::class, ModuleNewsDatabase::class])
interface RepositoryComponent {
    fun inject(viewModelFactory: ViewModelFactory)
}