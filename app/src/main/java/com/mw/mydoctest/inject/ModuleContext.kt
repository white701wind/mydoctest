package com.mw.alfanews.inject

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class ModuleContext(
    private val applicationContext: Context
) {
    @Provides
    @Named("ApplicationContext")
    fun getApplicationContext() = applicationContext
}