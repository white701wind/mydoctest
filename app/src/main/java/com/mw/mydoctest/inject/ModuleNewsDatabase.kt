package com.mw.alfanews.inject

import android.content.Context
import com.mw.mydoctest.core.repository.db.DatabaseClient
import com.mw.mydoctest.core.repository.db.RoomDatabaseClient
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module(includes = [ModuleContext::class])
class ModuleNewsDatabase {

    @Provides
    fun getNewsDatabase(@Named("ApplicationContext") applicationContext: Context): DatabaseClient = RoomDatabaseClient(applicationContext)
}