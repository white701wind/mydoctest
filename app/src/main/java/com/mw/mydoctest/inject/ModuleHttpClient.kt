package com.mw.alfanews.inject

import com.mw.mydoctest.core.repository.httpclient.HttpClient
import com.mw.mydoctest.core.repository.httpclient.RetrofitHttpClient
import dagger.Module
import dagger.Provides

@Module
class ModuleHttpClient {

    @Provides
    fun getHttpClient(): HttpClient = RetrofitHttpClient()
}