package com.mw.mydoctest.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mw.mydoctest.app.MyDocApplication
import com.mw.mydoctest.core.repository.Repository
import com.mw.mydoctest.core.repository.converter.ConverterApiToData
import com.mw.mydoctest.core.repository.db.RoomDatabaseClient
import com.mw.mydoctest.core.repository.httpclient.RetrofitHttpClient
import com.mw.mydoctest.ui.diagnostics.DiagnosticsViewModel
import com.mw.mydoctest.ui.services.ServicesViewModel
import javax.inject.Inject

class ViewModelFactory : ViewModelProvider.Factory {

    @Inject
    lateinit var repository: Repository

    init {
        MyDocApplication.get().getRepositoryComponent().inject(this)
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DiagnosticsViewModel::class.java)) {

//            MyDocApplication.get().getRepositoryComponent().inject(this)

            return DiagnosticsViewModel(repository) as T
        }else if (modelClass.isAssignableFrom(ServicesViewModel::class.java)) {

//            MyDocApplication.get().getRepositoryComponent().inject(this)

            return ServicesViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}